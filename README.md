Goldberg & Weigand, LLP is a personal injury law firm serving the injury victims of New Bedford and all of Bristol County. Peter M. Goldberg and Blair E. Weigand are committed to fighting for the car accident and personal injury victims of the Cape Cod area in Massachusetts.

Address: 460 County St, #2, New Bedford, MA 02740, USA

Phone: 508-961-2266

Website: https://capeinjurylaw.com/new-bedford-personal-injury-lawyer/